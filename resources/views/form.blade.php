<!DOCTYPE html>
<html>
<head>
    <title>Laravel Blog</title>
</head>
<body>
<p>
    <a href="{{ url('create_post') }}">Create New Post</a>
    &nbsp;&nbsp;
    <a href="{{ url('/') }}">Show all post</a>
</p>
<form action="{{ url('post_store') }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <p><input type="text" name="title" placeholder="Title Here"></p>
    <p>
        <textarea name="description" placeholder="Description Here"></textarea>
    </p>
    <p>Status : 
        <select name="status">
            <option value="posted">Select</option>
            <option value="draft" selected>Draft</option>
        </select>
    </p>
    <p><button type="submit" name="submit">Submit</button></p>
</form>
</body>
</html>