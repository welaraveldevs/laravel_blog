<!DOCTYPE html>
<html>
<head>
    <title>Laravel Blog</title>
</head>
<body>
<p>
    <a href="{{ url('create_post') }}">Create New Post</a>
    &nbsp;&nbsp;
    <a href="{{ url('/') }}">Show all post</a>
</p>
<form action="{{ url('post_update', $post->id) }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <p><input type="text" name="title" value="{{ $post->title }}" placeholder="Title Here"></p>
    <p>
        <textarea name="description" placeholder="Description Here">{{ $post->description }}</textarea>
    </p>
    <p>Status : 
        <select name="status">
            <option value="posted" @if($post->status == 'posted') selected @endif >Select</option>
            <option value="draft" @if($post->status == 'draft') selected @endif >Draft</option>
        </select>
    </p>
    <p><button type="submit" name="submit">Update</button></p>
</form>
</body>
</html>