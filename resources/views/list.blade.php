<!DOCTYPE html>
<html>
<head>
    <title>Laravel Blog</title>
</head>
<body>
	<p>
	    <a href="{{ url('create_post') }}">Create New Post</a>
	    &nbsp;&nbsp;
	    <a href="{{ url('/') }}">Show all post</a>
	</p>

	<table width="100%">
		<tr>
			<th align="left">Title</th>
			<th align="left">Description</th>
			<th align="left">Status</th>
			<th align="right">Option</th>
		</tr>
		@foreach($posts as $post)
		<tr>
			<td>{{ $post->title }}</td>
			<td>{{ $post->description }}</td>
			<td>{{ $post->status }}</td>
			<td align="right">
				<a href="{{ url('post_show', $post->id) }}">Show</a>
				&nbsp;&nbsp;
				<a href="{{ url('post_edit', $post->id) }}">Edit</a>
				&nbsp;&nbsp;
				<a href="{{ url('post_delete', $post->id) }}">Delete</a>
			</td>
		</tr>
		@endforeach
	</table>
</body>
</html>