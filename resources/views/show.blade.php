<!DOCTYPE html>
<html>
<head>
    <title>Laravel Blog</title>
</head>
<body>
<p>
    <a href="{{ url('create_post') }}">Create New Post</a>
    &nbsp;&nbsp;
    <a href="{{ url('/') }}">Show all post</a>
</p>
<form action="{{ url('post_update', $post->id) }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <p>Title : {{ $post->title }}</p>
    <p>Description : {{ $post->description }}</p>
    <p>Status : {{ $post->status }}</p>
</form>
<p>
    <a href="">Like (8)</a>
    &nbsp;&nbsp;
    <a href="">Comments (8)</a>
</p>
</body>
</html>