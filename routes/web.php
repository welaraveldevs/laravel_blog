<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', 'postController@index');
Route::get('create_post', 'postController@create');
Route::post('post_store', 'postController@store');
Route::get('post_edit/{id}', 'postController@edit');
Route::get('post_show/{id}', 'postController@show');
Route::post('post_update/{id}', 'postController@update');
Route::get('post_delete/{id}', 'postController@destroy');